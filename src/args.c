#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>


#define SYMS        (26)            // symbols in eng alphabetical
#define STAT        (SYMS+1)        // size of AB stats array
#define BUF_SIZE    (1024)          // file read buff size
#define KEY_LEN_MAX (32)
#define ENG_IDX     (0.065)         // comparsion index (CI) of eng dictionary
#define IDX_DELTA   (1.15)          // trusted region of CI
#define TRUST       (1.15)          // trusted delta of semantic in statistic of symbols
#define SEM_ERRORS  (9)             // available semantic errors


const char  sym[SYMS]      = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
const float eng_stat[SYMS] = {8.17,1.49,2.78,4.25,12.7,2.23,2.02,6.09,6.97,0.15,0.77,4.03,2.41,6.75,7.51,1.93,0.10,5.99,6.33,9.06,2.76,0.98,2.36,0.15,1.97,0.05};


/**
 * self named function, checking of symbol
 * @param ch  - in
 * @return : - out - 1 - if is alphabetical symbol; 0 - if not symbol
 */
int is_letter(char * ch) {
    if(*ch>='a' && *ch<='z') {
        return 1;
    }else if(*ch>='A' && *ch<='Z') {
        return 1;
    }
    return 0;
}


/**
 * self named function, checking of symbol register
 * @param ch - in - pointer to input symbol
 * @return  - out - 1 if upper, 0 - if no upper or not symbol
 */
int is_upper(char * ch) {
    if(*ch>='A' && *ch<='Z') {
        return 1;
    }
    return 0;    
}


/**
 * self named function, checking of symbol register
 * @param ch - in - pointer to input symbol
 * @return  - out - 1 if lower, 0 - if not lower or not symbol
 */
int is_lower(char * ch) {
    if(*ch>='a' && *ch<='z') {
        return 1;
    }
    return 0;    
}


/**
 * self named function, switching symbol to lower register
 * @param ch - in - pointer to input symbol
 * @return  - out - symbol in lower register
 */
char to_lower(char * ch) {
    char retval = *ch;
    if(*ch>='A' && *ch<='Z') {
        retval = *ch - 'A' + 'a';
    }
    return retval;
}


/**
 * self named function, 
 * @param key - in - encode key of password
 * @param ch  - in - char to decode
 * @return   - out - decoded char
 */
char decode_char(int key, char ch) {
    char retval = ch;

    if(is_upper(&ch))
        retval = (ch - 'A' + SYMS - key)%SYMS + 'A';
    else if(is_lower(&ch))
        retval = (ch - 'a' + SYMS - key)%SYMS + 'a';

    return retval;
}


/**
 * self named function, calculating of comparing index
 * @param _stats  - pointer to array with statistic of coded chars got from file
 * @return offset - its possible first char of key
 */
float index_calc(int *stats) {
    float index[STAT];
    int all_symb = 0;

    /*reset delta buffer*/
    memset(index, 0, sizeof(index));

    for (int j=0; j<SYMS; j++) {
        all_symb += *(stats+j);
    }
    for (int j=0; j<SYMS; j++) {
        /*calculate symbol statistic*/
        index[SYMS] += (float)(stats[j]*(stats[j]-1))/(float)(all_symb*(all_symb-1));
    }
    /*store raw number of minimal delta*/

//    printf("index=%f\n", index[SYMS]);
//    printf("symbols = %d\n", all_symb);
    return index[SYMS];
}


/**
 * self named function, analyze array of index
 * @param idx - in - float index array
 * @param len - in - key length checking
 * @return    - out - possible key length; return -1 if check not passed
 */
int check_index(float *idx, int len) {
    int retval = -1;
    int possible = 0;
    int wrong = 0;
    for(int i=0; i<len; i++) {
        /*is index inside trust interval*/
        if( (*(idx+i)*IDX_DELTA)>ENG_IDX && (*(idx+i)/IDX_DELTA)<ENG_IDX )
            possible++;
        else
            wrong++;
    }
    if(wrong < 1+len/5) {
        printf("possible key length = %d\n", len);
        retval = len;
    }
    return retval;
}


/**
 * self named function
 * @param fd, in - input file descriptor
 * @param fd, in - index statistic array
 * @return 
 */
int search_keylen(int fd, float * syms_stat) {
    int retval = -1;
    ssize_t ret_in;
    char buffer[BUF_SIZE];                              /* Character buffer */
    int symbols = 0;
    int sym_stat[STAT] = {0,0};                         /* symbols statistic buffer */
    float idx_stat[KEY_LEN_MAX] = {0,0};                /* indexes statistic */
    int len = 1;

    /* iterate available key lengths */
    while(len<=KEY_LEN_MAX && retval<1) {
        /* set key & read length */
        memset(idx_stat, 0, sizeof(idx_stat));
        /* set symbol statistic buffer */
        for(int ofs=0; ofs<len; ofs++) {
            /* Read process */
            lseek (fd, 0, SEEK_SET);
            /* reset symbol statistic buffer */
            memset(sym_stat, 0, sizeof(sym_stat));
            symbols = 0;
            int read_len = BUF_SIZE - BUF_SIZE%len;     /* cut read length with key length */
            while((ret_in = read (fd, &buffer, read_len)) > 0) {
                for(int i=0; i<ret_in; i++)
                    if(is_letter(buffer+i)) {
                        if((symbols%len) == ofs) {      /* decimation */
                            char curr = to_lower(buffer+i);
                            sym_stat[(int)(curr-'a')]++;
                            sym_stat[SYMS]++;           /* accumulate symbols coded with current keychar */
                        }
                        symbols++;                      /* symbol counter */
                    }
            }
            /* collect comparing index */
            idx_stat[ofs] = index_calc(sym_stat);
            /* save symbol statistic of N position */
            memcpy((syms_stat+ofs*STAT), sym_stat, sizeof(sym_stat));
        }
        retval = check_index(idx_stat, len);
        printf("key len=%d, result=%d\n", len, retval);
        len++;
    }

    return retval;
}


/***/
void build_semantic(const float * eng_alpha, int * semantic) {
    float prev = *(eng_alpha+SYMS-1);                                       // get last char freq
    for(int i=0; i<SYMS; i++) {
        *(semantic+i) = *(eng_alpha+i) > prev*TRUST ? 1 : *(semantic+i);    // 1 - mean frequency of current char is much more then previous
        *(semantic+i) = *(eng_alpha+i) < prev/TRUST ? -1 : *(semantic+i);   // -1 - mean freq. of current char is much less then prev.
        prev = *(eng_alpha+i);
//        printf("%d::%d\t", i, *(semantic+i));
    }
    printf("\n");
}


/***/
int char_semantic(float * stat, int * semantic) {
    int retval = '.';
    int pass = 0;
    int wrong = 0;
    int curr_semantic[SYMS];                            /* current chars AB semantic */
    float prev = *(stat+SYMS-1)/(*(stat+SYMS));
    memset(curr_semantic, 0, sizeof(curr_semantic));

    for(int i=0; i<SYMS; i++) {
        *(curr_semantic+i) = *(stat+i)/(*(stat+SYMS)) > prev*TRUST ? 1 : *(curr_semantic+i);
        *(curr_semantic+i) = *(stat+i)/(*(stat+SYMS)) < prev/TRUST ? -1 : *(curr_semantic+i);
        prev = *(stat+i)/(*(stat+SYMS));
    }

    for(int offset=0; offset<SYMS; offset++) {
        pass = 0;
        wrong = 0;
        for(int i=0; i<SYMS; i++) {
            if(*(semantic+i) && *(curr_semantic+(SYMS-offset+i)%SYMS) == *(semantic+i))
                pass++;
            else
                wrong++;
        }
        if(wrong <= SEM_ERRORS) {
            printf("offset=%d\tpassed=%d\twrong=%d\n", offset, pass, wrong);
//            printf("found offset %d\n", offset);
            retval = (SYMS-offset)%SYMS+'a';
            break;
        }            
    }
    return retval;
}


/***/
char symb_search(float * symb_stat) {
    char ch = 0;                                        /* return char value */
//    float current_stat;
//    float all_stat = 0;
//    float stat_max = -1;                              /* max stat value of symb_stat array */
    int stat_sz = *(symb_stat+SYMS);
    int eng_semantic[SYMS];                             /* eng chars AB semantic */

    memset(eng_semantic, 0, sizeof(eng_semantic));      // reset origin semantic
    build_semantic(eng_stat, eng_semantic);
    ch = char_semantic(symb_stat, eng_semantic);

    return ch;
}


/**
 * self named function
 * @param key_len   - in - key length
 * @param syms_stat - in - pointer to arrays of symbols statistics
 * @param keypass   - out - pointer to keypass
 */
void key_search(int key_len, float * syms_stat, char * keypass) {
    if(keypass==NULL || key_len==0)
        return;

    /* get keypass chars one by one */
    for(int i=0; i<key_len; i++) {
        *(keypass+i) = symb_search(syms_stat+i*STAT);
    }
}


/**
 * entry point
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char**argv) {
    int i;
    int input_fd, output_fd;                            /* Input and output file descriptors */
    ssize_t ret_in, ret_out;                            /* Number of bytes returned by read() and write() */
    int key_len = 0;
    float syms_stat[KEY_LEN_MAX*STAT];                    /* symbols statistic */
    char keypass[KEY_LEN_MAX];
    char buffer[BUF_SIZE];                              /* Character buffer */


    // Prints arguments
    printf("Arguments:\n");
    for(i = 0; i < argc; i++) {
        printf("%i: %s\n", i, argv[i]);
    }

    /* Are src and dest file name arguments missing */
    if(argc != 3) {
        printf ("Usage: ./args file_in file_out\n");
        return 1;
    }

    /* Create input file descriptor */
    input_fd = open (argv [1], O_RDONLY);
    if(input_fd == -1) {
        perror ("open");
        return 2;
    }

    /* Create output file descriptor */
    output_fd = open(argv[2], O_WRONLY | O_CREAT, 0644);
    if(output_fd == -1) {
        perror("open");
        return 3;
    }

    memset(syms_stat, 0, sizeof(syms_stat));
    key_len = search_keylen(input_fd, syms_stat);

    memset(keypass, 0, sizeof(keypass));
    if(key_len > 0) {
        key_search(key_len, syms_stat, keypass);
        printf("keypass - %s\n", keypass);
    }else {
        return (EXIT_FAILURE);
    }

    int read_len = BUF_SIZE - BUF_SIZE%key_len;
    lseek (input_fd, 0, SEEK_SET);
    while((ret_in = read (input_fd, &buffer, read_len)) > 0) {
        char curr;
        int symbols = 0;
        for(int i=0; i<ret_in; i++) {
            if(is_letter(buffer+i)) {
                curr = decode_char(keypass[symbols%key_len]-'a', *(buffer+i));
                symbols++;
            }else{
                curr = *(buffer+i);
            }
            *(buffer+i) = curr;
        }

        ret_out = write (output_fd, &buffer, (ssize_t) ret_in);
        if(ret_out != ret_in) {
            /* Write error */
            perror("write");
            return 4;
        }
    }

    /* Close file descriptors */
    close (input_fd);
    close (output_fd);

    return (EXIT_SUCCESS);
}
