VigenerBreaker
===============================

VigenereBreaker is command line application to find passphrase of coded text file with Vigenere cipher.


How it works
===============================

First step is to find length of passphrase with comparsion index of englesh language (CI = 0.065) with periodical repeated coded symbols with each symbol of passphrase.

Second - is to build alphabetical statistics for each char of passphrase.

Next step including building semantic of original alphabetical of english language (find rises and fales in statics like a function with guarantied or not parts of that function (1 - rised 1.15 times more? then prev symbol, -1 - falled 1.15 times of previous symbol, 0 - if its in [prev/1.15..prev*1.15] region))

After building original and research semantics - programm compare it with periodical shift (statistically - for long encoded text (1000 coded symbols for each passphrase char) - passed/wrong semantic >= 4 is enough to decide - coding symbol is right.

In complete at the end of output passphrase will be printed - if coding symbol not found in trusted regions - its replased with '.' in output.


Build
===============================
cd Arguments
make all


Usage
===============================
cd dist/Release/GNU_Linux/

./arguments [encoded.txt] [decoded.txt]

maximal passphrase lenght is 32 symbols, minimal length is 1, :)

output: keypass - abcd..gh


Example
===============================
Just add your *.txt file into samples directory or use pre-encoded sample-*.txt files.

user@host:~/NetBeansProjects/Arguments$ ./dist/Release/GNU-Linux/arguments samples/sample_6.txt out.txt
Arguments:
0: ./dist/Release/GNU-Linux/arguments
1: samples/sample_6.txt
2: out.txt
key len=1, result=-1
key len=2, result=-1
key len=3, result=-1
key len=4, result=-1
key len=5, result=-1
key len=6, result=-1
key len=7, result=-1
key len=8, result=-1
key len=9, result=-1
key len=10, result=-1
key len=11, result=-1
key len=12, result=-1
key len=13, result=-1
key len=14, result=-1
key len=15, result=-1
key len=16, result=-1
key len=17, result=-1
key len=18, result=-1
key len=19, result=-1
possible key length = 20
key len=20, result=20

offset=0	passed=21	wrong=5

offset=15	passed=22	wrong=4

offset=21	passed=20	wrong=6

offset=19	passed=21	wrong=5

offset=7	passed=21	wrong=5

offset=2	passed=21	wrong=5

offset=13	passed=22	wrong=4

offset=5	passed=20	wrong=6

offset=19	passed=22	wrong=4

offset=20	passed=21	wrong=5

offset=21	passed=20	wrong=6

offset=2	passed=20	wrong=6

offset=19	passed=21	wrong=5

offset=23	passed=21	wrong=5

offset=8	passed=21	wrong=5

offset=13	passed=22	wrong=4

offset=13	passed=22	wrong=4

offset=17	passed=20	wrong=6

offset=20	passed=21	wrong=5

offset=7	passed=22	wrong=4
keypass - alfhtynvhgfyhdsnnjgt

user@host:~/NetBeansProjects/Arguments/dist/Debug/GNU-Linux$